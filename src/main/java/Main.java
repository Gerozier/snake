import de.snake.gui.Gui;

public class Main {
    public static void main(String[] args) {
        Gui gui = new Gui();
        gui.create();
    }
}
