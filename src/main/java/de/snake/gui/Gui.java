package de.snake.gui;

import de.snake.actions.KeyHandler;

import javax.swing.*;

public class Gui {

    public static int width = 800, height = 600;

    public static int xoff = 130, yoff = 20;

    public void create () {

        JFrame jFrame = new JFrame("Snake");
        jFrame.setSize(width, height);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setLayout(null);
        jFrame.setResizable(false);
        jFrame.addKeyListener(new KeyHandler());

        Draw draw = new Draw();
        draw.setBounds(0,0, width, height);
        draw.setVisible(true);

        jFrame.add(draw);

        jFrame.requestFocus();
        jFrame.setVisible(true);
    }
}
