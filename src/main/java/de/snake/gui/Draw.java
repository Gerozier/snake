package de.snake.gui;

import de.snake.clocks.GameClock;
import de.snake.game.Snake;

import javax.swing.*;
import java.awt.*;

public class Draw extends JLabel {

    public static final int ITEMSIZE = 32;
    public static final int GRIDSTART = 0;
    public static final int GRIDEND = 15;
    public static final int GRIDSIZE = 16;

    public static boolean start = false;

    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        if (!start) {
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0,0, Gui.width, Gui.height);

            graphics.setColor(Color.RED);
            graphics.setFont(new Font("Arial", Font.BOLD, 20));
            graphics.drawString("Start with Enter", 300, 200);
        }else {

            // Draw Background
            graphics.setColor(new Color(119, 126, 127));
            graphics.fillRect(0, 0, Gui.width, Gui.height);

            // Draw Snake Tails
            graphics.setColor(new Color(51, 204, 51));
            Point point;
            for (int i = 0; i < Snake.tails.size(); i++) {
                point = Snake.ptc(Snake.tails.get(i).getX(), Snake.tails.get(i).getY());
                graphics.fillOval(point.x, point.y, ITEMSIZE, ITEMSIZE);
            }


            // Draw Snake Head
            graphics.setColor(new Color(0, 153, 0));
            point = Snake.ptc(Snake.head.getX(), Snake.head.getY());
            graphics.fillOval(point.x, point.y, ITEMSIZE, ITEMSIZE);


            // Draw PickUp
            if (Snake.score > 0 && Snake.score % 10 == 0){
                graphics.setColor(new Color((int) (Math.random() * 0x1000000)));
                point = Snake.ptc(Snake.pickUp.getX(), Snake.pickUp.getY());
                graphics.fillOval(point.x, point.y, ITEMSIZE, ITEMSIZE);
            }else {
                graphics.setColor(new Color(185, 187, 0));
                point = Snake.ptc(Snake.pickUp.getX(), Snake.pickUp.getY());
                graphics.fillOval(point.x, point.y, ITEMSIZE, ITEMSIZE);
            }


            // Draw Grid
            graphics.setColor(Color.GRAY);
            for (int i = 0; i < GRIDSIZE; i++) {
                for (int j = 0; j < GRIDSIZE; j++) {
                    graphics.drawRect(i * ITEMSIZE + Gui.xoff, j * ITEMSIZE + Gui.yoff, ITEMSIZE, ITEMSIZE);
                }

            }

            // Draw Border
            graphics.setColor(Color.BLACK);
            graphics.drawRect(Gui.xoff, Gui.yoff, 512, 512);

            // Draw Score and Snake Length
            graphics.setFont(new Font("Arial", Font.BOLD, 20));
            graphics.drawString("Score: " + Snake.score, 5, 25);
            graphics.drawString("Highscore: " + Snake.bestscore, 650, 25);
            graphics.drawString("Länge: " + Snake.tails.size(), 650, 50);

            // Draw Game Over and Restart
            if (!GameClock.running) {
                graphics.setColor(Color.WHITE);
                graphics.fillRect(0, 0, Gui.width, Gui.height);

                graphics.setColor(Color.RED);
                graphics.setFont(new Font("Arial", Font.BOLD, 20));
                graphics.drawString("Game Over", 325, 200);
                graphics.drawString("Score: " + Snake.score, 335, 240);

                if (Snake.score == Snake.bestscore) {
                    graphics.setColor(new Color((int) (Math.random() * 0x1000000)));
                    graphics.setFont(new Font("Arial", Font.BOLD, 45));
                    graphics.drawString("New Highscore !!!", 200, 320);
                }

                graphics.setColor(Color.BLUE);
                graphics.setFont(new Font("Arial", Font.BOLD, 20));
                graphics.drawString("Press Space for Restart", 275, 450);

            }

        }
        repaint();
    }

}
