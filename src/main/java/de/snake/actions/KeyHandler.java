package de.snake.actions;

import de.snake.clocks.GameClock;
import de.snake.game.Direction;
import de.snake.game.Snake;
import de.snake.gui.Draw;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements KeyListener {

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_W:
                if (!(Snake.head.getDirection() == Direction.DOWN) && !Snake.waitToMove) {
                    Snake.head.setDirection(Direction.UP);
                    Snake.waitToMove = true;
                }
                break;
            case KeyEvent.VK_A:
                if (!(Snake.head.getDirection() == Direction.RIGTH) && !Snake.waitToMove) {
                    Snake.head.setDirection(Direction.LEFT);
                    Snake.waitToMove = true;
                }
                break;
            case KeyEvent.VK_S:
                if (!(Snake.head.getDirection() == Direction.UP) && !Snake.waitToMove) {
                    Snake.head.setDirection(Direction.DOWN);
                    Snake.waitToMove = true;
                }
                break;
            case KeyEvent.VK_D:
                if (!(Snake.head.getDirection() == Direction.LEFT) && !Snake.waitToMove) {
                    Snake.head.setDirection(Direction.RIGTH);
                    Snake.waitToMove = true;
                }
                break;
            case KeyEvent.VK_SPACE:
                if (!GameClock.running) {
                    GameClock.running = true;
                    GameClock gc = new GameClock();
                    gc.start();
                }
                break;
            case KeyEvent.VK_ENTER:
                Draw.start = true;
                GameClock gameClock = new GameClock();
                gameClock.start();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
