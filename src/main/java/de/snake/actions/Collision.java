package de.snake.actions;

import de.snake.game.Snake;
import de.snake.game.Tail;
import de.snake.gui.Draw;

import java.util.ArrayList;

public class Collision {

    public static boolean collideSelf() {
        for (int i = 0; i < Snake.tails.size(); i++) {
            if (Snake.head.getX() == Snake.tails.get(i).getX()
                    && Snake.head.getY() == Snake.tails.get(i).getY()
                    && !Snake.tails.get(i).isWait()) {
                return true;
            }
        }
        return false;
    }

    public static boolean collideWall() {
        return (Snake.head.getX() < Draw.GRIDSTART || Snake.head.getX() > Draw.GRIDEND || Snake.head.getY() < Draw.GRIDSTART || Snake.head.getY() > Draw.GRIDEND);
    }

    public static void collidePickUp() {
        if (checkIfSnakeCollide()) {
            Snake.pickUp.reset();
            Snake.addTail();
            Snake.score += 1;
            if (Snake.score > Snake.bestscore) {
                Snake.bestscore = Snake.score;
            }
        }
    }

    public static boolean checkIfSnakeCollide() {
        return Snake.tails.stream().anyMatch(Collision::check) || Snake.head.getX() == Snake.pickUp.getX() && Snake.head.getY() == Snake.pickUp.getY();
    }

    private static boolean check(Tail tail) {
        return tail.getX() == Snake.pickUp.getX() && tail.getY() == Snake.pickUp.getY();
    }
}
