package de.snake.clocks;

import de.snake.actions.Collision;
import de.snake.game.Snake;

public class GameClock extends Thread{

    public static boolean running = true;

    private static final int GAMESPEED = 150;

    public void run() {
        Snake.score = 0;
        while (running) {
            try {
                sleep(GAMESPEED);
                Snake.move();
                Snake.waitToMove = false;
                Collision.collidePickUp();
                if (Collision.collideSelf()) {
                    Snake.tails.clear();
                    running = false;
                }
                if (Collision.collideWall()) {
                    Snake.tails.clear();
                    Snake.head.setX(7);
                    Snake.head.setY(7);
                    running = false;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
